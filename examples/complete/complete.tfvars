namespace   = "cp"
environment = "uw2"
stage       = "prod"
name        = "null-label"

delimiter       = ""
id_length_limit = 6

label_key_case   = "lower"
label_value_case = "upper"
