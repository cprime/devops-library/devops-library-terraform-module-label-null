module gitlab.com/cprime/devops-library/devops-library-terraform-module-project-template-aws

go 1.13

require (
	github.com/gruntwork-io/terratest v0.31.1
	github.com/qdm12/reprint v0.0.0-20200326205758-722754a53494
	github.com/stretchr/testify v1.5.1
)
